**Poly-chat**

This is a really simple chat.
To get started checkout the `start` branch.
 Run `npm install` and `bower install`.
 If you have polymer-cli installed you can start the app with `polymer serve`.
 
 The app make use of PubHub for the backend.
 
**1. Create a polymer element to display the message**

Create a new element `chat-message` to show the messages with a common template. 

Let the message display `You` as name if the username message is the same as the user.

**2. Let the user choose the name**

The name of the user is now choose randomly. Create a simple dialog to let the user choose the name. 